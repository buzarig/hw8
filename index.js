"use strict";

// TASK 1
let paragraph = document.querySelectorAll("p");

paragraph.forEach((e) => {
  e.style.background = "blue";
});

// TASK 2
let options = document.getElementById("optionsList");
console.log(options);
console.log(options.parentElement);
console.log(options.childNodes, typeof options.childNodes);

// TASK 3
const testPar = (document.querySelector("#testParagraph").innerHTML =
  "This is a paragraph");

console.log(testPar);

// TASK 4
let headerElements = document.querySelector(".main-header").childNodes;
for (let key of headerElements) {
  key.className = "nav-item";
  console.log(key);
}

// TASK 5
const titles = document.querySelectorAll(".section-title");
const snapshot = Array.from(titles);

const main = snapshot.forEach((e) => {
  e.classList.remove("section-title");
});
